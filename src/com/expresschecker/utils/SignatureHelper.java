package com.expresschecker.utils;

import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class SignatureHelper {
	private static final String HMAC_SHA1 = "HmacSHA1"; 
	//private static final String signedKey = "1234567890";
	
	public static String printHexString( byte[] b) {  
		String hexResult = "";
		for (int i = 0; i < b.length; i++) { 
			String hex = Integer.toHexString(b[i] & 0xFF); 
			if (hex.length() == 1) { 
				hex = '0' + hex; 
			}
			hexResult += hex;
//			System.out.print(hex.toUpperCase() ); 
		}
		
		return hexResult;
	}
	
	public static String getHmacSHA1Signature(byte[] data, byte[] key){  
        SecretKeySpec signingKey = new SecretKeySpec(key, HMAC_SHA1);  
        Mac mac;
		try {
			mac = Mac.getInstance(HMAC_SHA1);
			mac.init(signingKey);  
	        byte[] rawHmac = mac.doFinal(data);  
//	        System.out.println("size=" + rawHmac.length + " rawHmac=" + rawHmac.toString());
	        
			String signature = printHexString(rawHmac);
//			System.out.print("signature=" + signature);
			return signature;  
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }  
	
	public static String produceSignature(String path, Map<String, String> paramsMap, byte[] body, String signedKey) {
		if (path == null){
			return null;
		}
		String params = "";
		if (paramsMap != null) {
			Set<String> keySet = paramsMap.keySet();
			for(String key:keySet) {
				if(params.length() != 0) {
					params += "&";
				}
				String value = paramsMap.get(key);
				params += key + "=" + value;
			}
		}
		byte[] pathData = null;
		if (params.length() > 0) {
			pathData = (path + "?" + params).getBytes();
		} else {
			pathData = path.getBytes();
		}
		byte[] data = null;
		if (body != null && body.length > 0) {
			data = new byte[pathData.length + body.length];
			System.arraycopy(pathData, 0, data, 0, pathData.length);
			System.arraycopy(body, 0, data, pathData.length, body.length);
		} else {
			data = pathData;
		}
		String signature = getHmacSHA1Signature(data, signedKey.getBytes());
		return signature;
	}
	
	public static String produceSignature(String fullPath, byte[] body, String signedKey) {
		if (fullPath == null || body == null) {
			return null;
		}
		byte[] pathData = fullPath.getBytes();
		byte[] data = new byte[pathData.length + body.length];
		System.arraycopy(pathData, 0, data, 0, pathData.length);
		System.arraycopy(body, 0, data, pathData.length, body.length);
		String signature = getHmacSHA1Signature(data, signedKey.getBytes());
		return signature;
	}
	
	public static String produceSignature(String fullPath, String signedKey) {
		if (fullPath == null) {
			return null;
		}
		byte[] pathData = fullPath.getBytes();
		String signature = getHmacSHA1Signature(pathData, signedKey.getBytes());
		return signature;
	}
}
