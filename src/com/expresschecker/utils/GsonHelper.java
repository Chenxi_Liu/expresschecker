package com.expresschecker.utils;

import com.google.gson.Gson;

public class GsonHelper {
	private static Object getGsonInstanceLock = new Object();
	private static Gson gson;
	private static Gson getInstance(){
		if(gson == null){
			synchronized (getGsonInstanceLock) {
				if(gson == null){
					gson = new Gson();
				}
			}
		}
		return gson;
	}
	
	public static String trans2JsonStr(Object obj) {
		return getInstance().toJson(obj);
	}
	
	public static <T> Object trans2Object(String jsonStr, Class<T> classOfT) {
		return getInstance().fromJson(jsonStr, classOfT);
	}
}
