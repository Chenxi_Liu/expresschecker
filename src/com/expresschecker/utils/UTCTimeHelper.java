package com.expresschecker.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class UTCTimeHelper {
	public static long getUTCTime(){
		Calendar cal = Calendar.getInstance(); 
		
		if (cal == null) {
			return -1;
		}
		// 2、取得时间偏移量
        int zoneOffset = cal.get(java.util.Calendar.ZONE_OFFSET);  
        // 3、取得夏令时差
        int dstOffset = cal.get(java.util.Calendar.DST_OFFSET); 
		// 4、从本地时间里扣除这些差量，即可以取得UTC时间
        cal.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));

        return cal.getTimeInMillis();
	}
	
	public static long transYMDDate2UTCTime(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		Date dt;
		try {
			dt = sdf.parse(date);
			Calendar cal = Calendar.getInstance();  
			cal.setTime(dt);
			// 2、取得时间偏移量
	        int zoneOffset = cal.get(java.util.Calendar.ZONE_OFFSET);  
	        // 3、取得夏令时差
	        int dstOffset = cal.get(java.util.Calendar.DST_OFFSET); 
			// 4、从本地时间里扣除这些差量，即可以取得UTC时间
	        cal.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
	        return cal.getTimeInMillis();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		} 
	}
	
	public static Calendar transUTCTime2LocalDate(Long utcTime) {
		Calendar localCal = Calendar.getInstance(); 
		// 2、取得时间偏移量
        int zoneOffset = localCal.get(java.util.Calendar.ZONE_OFFSET);  
        // 3、取得夏令时差
        int dstOffset = localCal.get(java.util.Calendar.DST_OFFSET); 
        
		Calendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
		cal.setTimeInMillis(utcTime);
		cal.add(java.util.Calendar.MILLISECOND, zoneOffset + dstOffset);
		
		return cal;
	}
}
