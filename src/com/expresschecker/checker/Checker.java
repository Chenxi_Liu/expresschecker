package com.expresschecker.checker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.expresschecker.data.ExpressInfo;
import com.expresschecker.data.ExpressTrace;
import com.expresschecker.data.KDNiaoExpressInfo;
import com.expresschecker.data.KDNiaoExpressInfo.Trace;
import com.expresschecker.data.UnfinisedOrderExpressInfoList.UnfinisedOrderExpressInfo;
import com.expresschecker.data.ResultResp;
import com.expresschecker.data.UnfinisedOrderExpressInfoList;
import com.expresschecker.kdgoldapiquery.KDGoldAPIQuery;
import com.expresschecker.utils.GsonHelper;
import com.expresschecker.utils.SignatureHelper;
import com.expresschecker.utils.UTCTimeHelper;
import com.google.gson.JsonObject;

public class Checker {
	KDGoldAPIQuery mKDG;
	static private int PAGE_SIZE = 20;
	
//	static private String UNFINISHED_ORDER_EXPRESS_INFOS_GET_URL = "http://localhost:8080/GetUnfinishedOrderExpressInfos";
//	
//	static private String EXPRESS_INFO_POST_URL = "http://localhost:8080/ExpressInfo";
	
	static private String UNFINISHED_ORDER_EXPRESS_INFOS_GET_URL = "http://120.27.120.241:3003/MZEBusiness/GetUnfinishedOrderExpressInfos";
	
	static private String EXPRESS_INFO_POST_URL = "http://120.27.120.241:3003/MZEBusiness/ExpressInfo";
	
	static private int CHECK_INTERVAL = 10 * 60 * 1000;
	
	static private int EXPRESS_UPDATE_INTERVAL = 2 * CHECK_INTERVAL;
	
	public Checker() {
		mKDG = new KDGoldAPIQuery();
	}
	
	public void check() throws Exception {
		while(true) {
			int pageNo = 0;
			int pageSize = PAGE_SIZE;
			while(true) {
				long curTime = UTCTimeHelper.getUTCTime();
				UnfinisedOrderExpressInfoList list = sendExpressInfoGet(pageNo, pageSize);
				if (list != null && list.getCnt() != 0 && list.getUnfinisedOrderExpressInfoList() != null) {
					List<UnfinisedOrderExpressInfo> infoList = list.getUnfinisedOrderExpressInfoList();
					for (UnfinisedOrderExpressInfo info : infoList) {
						if (curTime - info.getUpdateTime() <= EXPRESS_UPDATE_INTERVAL) {
							continue;
						}
						String tracesStr = mKDG.getOrderTracesByJson(info.getExpressCode(), info.getTrackingNo());
						if (tracesStr != null && tracesStr.length() != 0) {
							String result = sendExpressInfoPost(tracesStr);
							if (result == null || result.length() == 0) {
								System.out.println("check sendExpressInfoPost fail!!!");
							} else {
								System.out.println("check sendExpressInfoPost result:" + result);
							}
						}
						Thread.sleep(100);
					}
				} else {
					System.out.println("check fail by null UnfinisedOrderExpressInfoList!!!");
					break;
				}
				
				if (list.getCnt() < pageSize) {
					System.out.println("check end for one checking loop");
					break;
				} else {
					pageNo += 1;
				}
			}
			//Each 
			Thread.sleep(CHECK_INTERVAL);
		}
	}
	
	private String sendGet(int pageNo, int pageSize) {  
		PrintWriter out = null;  
        BufferedReader in = null;  
        StringBuffer buffer = new StringBuffer();
        {            
        	HttpURLConnection con = null;
	        try {
	        	String params = "?pageNo=" + pageNo + "&pageSize=" + pageSize;
	        	String sig = getSignature("/GetUnfinishedOrderExpressInfos" + params, null);
	        	if (sig == null) {
	        		return null;
	        	}
	        	URL u = new URL(UNFINISHED_ORDER_EXPRESS_INFOS_GET_URL + params + "&signature=" + sig);
	
	        	con = (HttpURLConnection) u.openConnection();
	        	con.setRequestMethod("GET");
//	        	con.setDoOutput(true);
	        	con.setDoInput(true);
	        	con.setUseCaches(false);
	        	con.setRequestProperty("accept", "*/*");  
	        	con.setRequestProperty("connection", "Keep-Alive");  
	        	con.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");  
	        	con.connect();
	        	
	        	BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		        String temp;
		        while ((temp = br.readLine()) != null) {
			        buffer.append(temp);
		        }
	        } catch (Exception e) {
	        	e.printStackTrace();
	        } finally {
	        	if (con != null) {
	        		con.disconnect();
	        	}
	        }

	        return buffer.toString();
        } 
    }
	
	private String sendPost(String body) {  
        PrintWriter out = null;  
        BufferedReader in = null;  
        StringBuffer buffer = new StringBuffer();
        {            
        	HttpURLConnection con = null;
	        try {
	        	String sig = getSignature("/ExpressInfo", body);
	        	if (sig == null) {
	        		return null;
	        	}
	        	URL u = new URL(EXPRESS_INFO_POST_URL + "?signature=" + sig);
	
	        	con = (HttpURLConnection) u.openConnection();
	        	con.setRequestMethod("POST");
	        	con.setDoOutput(true);
	        	con.setDoInput(true);
	        	con.setUseCaches(false);
//	        	con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        	con.setRequestProperty("Content-Type", "application/octet-stream");
	        	con.setRequestProperty("Content-length", "" + body.getBytes().length);
	        	OutputStreamWriter osw = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
	        
	            System.out.println("body:" + body);
//	            osw.write(body.getBytes());
	        	osw.write(body);
	        	osw.flush();
	        	osw.close();
	        	
	        	BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		        String temp;
		        while ((temp = br.readLine()) != null) {
			        buffer.append(temp);
		        }
	        } catch (Exception e) {
	        	e.printStackTrace();
	        } finally {
	        	if (con != null) {
	        		con.disconnect();
	        	}
	        }

	        return buffer.toString();
        }
    }
	
	private String getSignature(String url, String body) {
		String sig;
		try {
			if (body == null) {
				sig = SignatureHelper.produceSignature(url, "1234567890");
			} else {
				sig = SignatureHelper.produceSignature(url, body.getBytes("utf-8"), "1234567890");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	 	return sig;
	}
	
	private UnfinisedOrderExpressInfoList sendExpressInfoGet(int pageNo, int pageSize) {
		String jsonStr = sendGet(pageNo, pageSize);
		if (jsonStr == null || jsonStr.length() == 0) {
			return null;
		}
		
		System.out.println("sendExpressInfoGet jsonStr:" + jsonStr);
		
		ResultResp resp = (ResultResp) GsonHelper.trans2Object(jsonStr, ResultResp.class);
		if (resp == null) {
			System.out.println("sendExpressInfoGet fail to parse response");
			return null;
		} else if (resp.getCode() != 0) {
			System.out.println("sendExpressInfoGet resp.code = " + resp.getCode() + " resp.msg = " + resp.getMsg());
			return null;
		} else if (resp.getResult() == null || resp.getResult().length() == 0) {
			System.out.println("sendExpressInfoGet not find result string from response");
			return null;
		}
		
		UnfinisedOrderExpressInfoList list
			= (UnfinisedOrderExpressInfoList) GsonHelper.trans2Object(resp.getResult(), UnfinisedOrderExpressInfoList.class);
		if (resp == null) {
			System.out.println("sendExpressInfoGet fail to parse result");
			return null;
		}
		
		return list;
	}
	
	private String sendExpressInfoPost(String infoStr) {
		if (infoStr == null) {
			return null;
		}
		ExpressInfo expressInfo = xferToExpressInfoObj(infoStr);
		if (expressInfo == null) {
			return null;
		}
		String body = GsonHelper.trans2JsonStr(expressInfo);
		if (body == null) {
			return null;
		}
		return sendPost(body);
	}
	
	private ExpressInfo xferToExpressInfoObj(String infoStr) {
		ExpressInfo expressInfo = null;
		try {
			KDNiaoExpressInfo info = (KDNiaoExpressInfo) GsonHelper.trans2Object(infoStr, KDNiaoExpressInfo.class);
			expressInfo = new ExpressInfo();
			expressInfo.setTrackingNo(info.getLogisticCode());
			if (info.getSuccess()) {
				expressInfo.setSuccess(1);
			} else {
				expressInfo.setSuccess(0);
			}
			expressInfo.setReason(info.getReason());
			expressInfo.setState(Integer.parseInt(info.getState()));
			
			List<ExpressTrace> expressTraceList = new ArrayList<ExpressTrace>();
			for (KDNiaoExpressInfo.Trace trace : info.getTraces()) {
				ExpressTrace expressTrace = new ExpressTrace();
				expressTrace.setAcceptTime(trace.getAcceptTime());
				expressTrace.setAcceptStation(trace.getAcceptStation());
				expressTrace.setRemark(trace.getRemark());
				expressTraceList.add(expressTrace);
			}
			expressInfo.setExpressTraceList(expressTraceList);
			
			return expressInfo;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String args[]) {
    	try {
    		Checker checker = new Checker();
    		checker.check();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
