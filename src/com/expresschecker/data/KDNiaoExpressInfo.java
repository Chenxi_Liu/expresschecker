package com.expresschecker.data;

import java.util.List;

public class KDNiaoExpressInfo {
	String EBusinessID;
	
	String OrderCode;
	
	String ShipperCode;
	
	String LogisticCode;
	
	Boolean Success;
	
	String Reason;
	
	String State;
	
	List<Trace> Traces;
	
	public String getEBusinessID() {
		return EBusinessID;
	}

	public void setEBusinessID(String eBusinessID) {
		EBusinessID = eBusinessID;
	}

	public String getOrderCode() {
		return OrderCode;
	}

	public void setOrderCode(String orderCode) {
		OrderCode = orderCode;
	}

	public String getShipperCode() {
		return ShipperCode;
	}

	public void setShipperCode(String shipperCode) {
		ShipperCode = shipperCode;
	}

	public String getLogisticCode() {
		return LogisticCode;
	}

	public void setLogisticCode(String logisticCode) {
		LogisticCode = logisticCode;
	}

	public Boolean getSuccess() {
		return Success;
	}

	public void setSuccess(Boolean success) {
		Success = success;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public List<Trace> getTraces() {
		return Traces;
	}

	public void setTraces(List<Trace> traces) {
		Traces = traces;
	}

	public class Trace {
		String AcceptTime;
		
		String AcceptStation;
		
		String Remark;

		public String getAcceptTime() {
			return AcceptTime;
		}

		public void setAcceptTime(String acceptTime) {
			AcceptTime = acceptTime;
		}

		public String getAcceptStation() {
			return AcceptStation;
		}

		public void setAcceptStation(String acceptStation) {
			AcceptStation = acceptStation;
		}

		public String getRemark() {
			return Remark;
		}

		public void setRemark(String remark) {
			Remark = remark;
		}
	}
}
