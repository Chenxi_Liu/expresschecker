package com.expresschecker.data;

import java.util.List;

public class ExpressInfo {
	String trackingNo;
	
	int success;
	
	String reason;
	
	int state;
	
	int tracesStep;
	
	List<ExpressTrace> expressTraceList;

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getTracesStep() {
		return tracesStep;
	}

	public void setTracesStep(int tracesStep) {
		this.tracesStep = tracesStep;
	}

	public List<ExpressTrace> getExpressTraceList() {
		return expressTraceList;
	}

	public void setExpressTraceList(List<ExpressTrace> expressTraceList) {
		this.expressTraceList = expressTraceList;
	}
}
