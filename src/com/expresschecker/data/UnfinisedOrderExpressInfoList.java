package com.expresschecker.data;

import java.util.List;

public class UnfinisedOrderExpressInfoList {
	int cnt;
	
	List<UnfinisedOrderExpressInfo> unfinisedOrderExpressInfoList;
	
	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public List<UnfinisedOrderExpressInfo> getUnfinisedOrderExpressInfoList() {
		return unfinisedOrderExpressInfoList;
	}

	public void setUnfinisedOrderExpressInfoList(List<UnfinisedOrderExpressInfo> unfinisedOrderExpressInfoList) {
		this.unfinisedOrderExpressInfoList = unfinisedOrderExpressInfoList;
	}

	public class UnfinisedOrderExpressInfo {		
		String trackingNo;
		
		String expressCode;
		
		long updateTime;

		public String getTrackingNo() {
			return trackingNo;
		}

		public void setTrackingNo(String trackingNo) {
			this.trackingNo = trackingNo;
		}

		public String getExpressCode() {
			return expressCode;
		}

		public void setExpressCode(String expressCode) {
			this.expressCode = expressCode;
		}

		public long getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(long updateTime) {
			this.updateTime = updateTime;
		}
	}
}
